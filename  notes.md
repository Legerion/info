Є два способа як зробити новий проект.
1. Создати новий репозиторий в GitLab, потім його склонувати черз git clone і прописати "url" проекта.

git switch --create main - змінити гілку з master на main
git add - додати файли
   git commit -m "  " закомитити зміни
   git push --set-upstream origin main - відправити в репозиторий
2. Надислати наявну папку
   git init - інстализуємо проект
   git remote add origin "url"( gitlab -звязуємо. куди будемо відправляти)
   git switch --create main - змінити гілку з master на main
   git add . - додати файли
   git commit -m "Initial commit" - закомитити зміни
   git push --set-upstream origin main - відправити в репозиторий